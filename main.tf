###################################################################################################
## If Cloudwatch watch log group is used as destination,
## the below block will be used. Otherwise, we use Cloudposse module for S3 as destination
###################################################################################################
resource "aws_flow_log" "cw_log_group" {
  count                    = var.log_destination_type == "cloud-watch-logs" ? 1 : 0
  vpc_id                   = var.vpc_id
  eni_id                   = var.eni_id
  subnet_id                = var.subnet_id
  iam_role_arn             = aws_iam_role.flowlog_assume_role[0].arn
  traffic_type             = var.traffic_type
  log_destination          = aws_cloudwatch_log_group.default[0].arn
  log_destination_type     = "cloud-watch-logs"
  log_format               = var.log_format
  max_aggregation_interval = var.max_aggregation_interval
  tags                     = var.tags
}

resource "aws_cloudwatch_log_group" "default" {
  count             = var.log_destination_type == "cloud-watch-logs" ? 1 : 0
  name              = var.cloudwatch_log_group_name
  retention_in_days = var.cloudwatch_log_group_retention_in_days
  kms_key_id        = var.cloudwatch_log_group_kms_key_id
  tags              = var.cloudwatch_log_tags
}

resource "aws_iam_role" "flowlog_assume_role" {
  count = var.log_destination_type == "cloud-watch-logs" ? 1 : 0
  name  = var.flow_log_iam_role_name

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpc-flow-logs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "post_to_cloudwatch" {
  count = var.log_destination_type == "cloud-watch-logs" ? 1 : 0

  name = var.flow_log_iam_policy_name
  role = aws_iam_role.flowlog_assume_role[0].id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

###################################################################################################
# Use Cloudposse module for S3 as destination. Only work on VPC level
###################################################################################################
module "flow_log_s3" {
  count         = var.log_destination_type == "s3" ? 1 : 0
  source        = "cloudposse/vpc-flow-logs-s3-bucket/aws"
  version       = "1.0.1"
  namespace     = var.namespace
  stage         = var.stage
  name          = "vpc-flow-logs-${var.s3_bucket_name}"
  vpc_id        = var.vpc_id
  traffic_type  = var.traffic_type
  force_destroy = var.s3_bucket_force_destroy
}
