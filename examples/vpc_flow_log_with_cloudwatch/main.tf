locals {
  namespace = "keltio"
  stage     = "test"
  name      = "app"
}


module "vpc" {
  source     = "cloudposse/vpc/aws"
  version    = "0.27.0"
  namespace  = "keltio"
  stage      = "test"
  name       = "app"
  cidr_block = "10.0.0.0/16"
}

module "flowlog_cw_log_group" {
  source                                 = "../.."
  vpc_id                                 = module.vpc.vpc_id
  namespace                              = local.namespace
  stage                                  = local.stage
  traffic_type                           = "ALL"
  log_destination_type                   = "cloud-watch-logs"
  cloudwatch_log_group_retention_in_days = 7
}
