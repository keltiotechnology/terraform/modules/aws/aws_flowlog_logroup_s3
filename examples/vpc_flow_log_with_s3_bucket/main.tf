locals {
  namespace = "keltio"
  stage     = "test"
  name      = "app"
}

module "vpc" {
  source     = "cloudposse/vpc/aws"
  version    = "0.27.0"
  namespace  = local.namespace
  stage      = local.stage
  name       = local.name
  cidr_block = "10.0.0.0/16"
}

module "flowlog_s3" {
  source                  = "../.."
  namespace               = local.namespace
  stage                   = local.stage
  vpc_id                  = module.vpc.vpc_id
  s3_bucket_name          = "Flowlog"
  s3_bucket_force_destroy = true
  traffic_type            = "ALL"
  log_destination_type    = "s3"
}
