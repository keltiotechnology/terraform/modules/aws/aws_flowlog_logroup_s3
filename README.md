# AWS Flowlog with Cloudwatch Log group or S3 Bucket

## Description

This module provides AWS Flowlog with either destination type is Cloudwatch log group or S3 Bucket.

- If using Cloudwatch log group as destination type, we can optional set Flowlog at 3 different level: VPC/Subnet/ENI.

- If using S3 as destination type, we can only set Flowlog at VPC level.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->


## Usage

```hcl
locals {
  namespace = "keltio"
  stage     = "test"
  name      = "app"
}


module "vpc" {
  source     = "cloudposse/vpc/aws"
  version    = "0.27.0"
  namespace  = "keltio"
  stage      = "test"
  name       = "app"
  cidr_block = "10.0.0.0/16"
}

module "flowlog_cw_log_group" {
  source                                 = "../.."
  vpc_id                                 = module.vpc.vpc_id
  namespace                              = local.namespace
  stage                                  = local.stage
  traffic_type                           = "ALL"
  log_destination_type                   = "cloud-watch-logs"
  cloudwatch_log_group_retention_in_days = 7
}

```



## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.61.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cloudwatch_log_group_kms_key_id"></a> [cloudwatch\_log\_group\_kms\_key\_id](#input\_cloudwatch\_log\_group\_kms\_key\_id) | The ARN of the KMS Key to use when encrypting log data | `string` | `null` | no |
| <a name="input_cloudwatch_log_group_name"></a> [cloudwatch\_log\_group\_name](#input\_cloudwatch\_log\_group\_name) | Name of the cloudwatch log group to be created if CloudWatch LogGroup is used as Flow Log destination | `string` | `"Flowlog"` | no |
| <a name="input_cloudwatch_log_group_retention_in_days"></a> [cloudwatch\_log\_group\_retention\_in\_days](#input\_cloudwatch\_log\_group\_retention\_in\_days) | Number of days to retent log events in Cloudwatch Log group. Possible values are: 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, 3653, and 0. If you select 0, the events in the log group are always retained and never expire | `number` | `0` | no |
| <a name="input_cloudwatch_log_tags"></a> [cloudwatch\_log\_tags](#input\_cloudwatch\_log\_tags) | Tags assigned to Cloudwatch log group | `map(string)` | `{}` | no |
| <a name="input_eni_id"></a> [eni\_id](#input\_eni\_id) | Elastic Network Interface ID to attach to | `string` | `null` | no |
| <a name="input_flow_log_iam_policy_name"></a> [flow\_log\_iam\_policy\_name](#input\_flow\_log\_iam\_policy\_name) | Name of the IAM policy to be created. It allows to post flow logs to CloudWatch Log group | `string` | `"flowlog_policy"` | no |
| <a name="input_flow_log_iam_role_name"></a> [flow\_log\_iam\_role\_name](#input\_flow\_log\_iam\_role\_name) | Name of the IAM role to be created. It allows to post flow logs to CloudWatch Log group | `string` | `"flowlog_role"` | no |
| <a name="input_log_destination_type"></a> [log\_destination\_type](#input\_log\_destination\_type) | The type of the logging destination. Valid values: cloud-watch-logs, s3 | `string` | `"cloud-watch-logs"` | no |
| <a name="input_log_format"></a> [log\_format](#input\_log\_format) | The fields to include in the flow log record, in the order in which they should appear. | `string` | `""` | no |
| <a name="input_max_aggregation_interval"></a> [max\_aggregation\_interval](#input\_max\_aggregation\_interval) | The maximum interval of time during which a flow of packets is captured and aggregated into a flow log record. Valid Values: 60 seconds (1 minute) or 600 seconds (10 minutes) | `number` | `600` | no |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | n/a | `string` | n/a | yes |
| <a name="input_s3_bucket_force_destroy"></a> [s3\_bucket\_force\_destroy](#input\_s3\_bucket\_force\_destroy) | Force destroy S3 Bucket | `string` | `false` | no |
| <a name="input_s3_bucket_name"></a> [s3\_bucket\_name](#input\_s3\_bucket\_name) | Name of the S3 Bucket to be created if S3 Bucket is used as Flow Log destination | `string` | `""` | no |
| <a name="input_stage"></a> [stage](#input\_stage) | n/a | `string` | n/a | yes |
| <a name="input_subnet_id"></a> [subnet\_id](#input\_subnet\_id) | Subnet ID to attach to | `string` | `null` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Key-value map of resource tags. If configured with a provider default\_tags configuration block present, tags with matching keys will overwrite those defined at the provider-level. | `map(string)` | `{}` | no |
| <a name="input_traffic_type"></a> [traffic\_type](#input\_traffic\_type) | The type of traffic to capture. Valid values: ACCEPT,REJECT, ALL | `string` | `"ALL"` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | VPC ID to attach to | `string` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cw_loggroup_arn"></a> [cw\_loggroup\_arn](#output\_cw\_loggroup\_arn) | ARN of Cloudwatch Log group |
| <a name="output_cw_loggroup_kms_key_id"></a> [cw\_loggroup\_kms\_key\_id](#output\_cw\_loggroup\_kms\_key\_id) | ARN of KMS Key to use when encrypting log data of Cloudwatch Log group |
| <a name="output_cw_loggroup_name"></a> [cw\_loggroup\_name](#output\_cw\_loggroup\_name) | Name of Cloudwatch Log group |
| <a name="output_cw_loggroup_name_prefix"></a> [cw\_loggroup\_name\_prefix](#output\_cw\_loggroup\_name\_prefix) | Name prefix of Cloudwatch Log group |
| <a name="output_cw_loggroup_retention_in_days"></a> [cw\_loggroup\_retention\_in\_days](#output\_cw\_loggroup\_retention\_in\_days) | Rentention days of Cloudwatch Log group |
| <a name="output_cw_loggroup_tags"></a> [cw\_loggroup\_tags](#output\_cw\_loggroup\_tags) | Tags of Cloudwatch Log group |
| <a name="output_flow_log_arn"></a> [flow\_log\_arn](#output\_flow\_log\_arn) | The ARN of the Flow Log |
| <a name="output_flow_log_id"></a> [flow\_log\_id](#output\_flow\_log\_id) | The Cloudwatch Flow Log ID |
| <a name="output_s3_bucket_arn"></a> [s3\_bucket\_arn](#output\_s3\_bucket\_arn) | Bucket ARN |
| <a name="output_s3_bucket_domain_name"></a> [s3\_bucket\_domain\_name](#output\_s3\_bucket\_domain\_name) | FQDN of bucket |
| <a name="output_s3_bucket_id"></a> [s3\_bucket\_id](#output\_s3\_bucket\_id) | Bucket Name (aka ID) |
| <a name="output_s3_bucket_notifications_sqs_queue_arn"></a> [s3\_bucket\_notifications\_sqs\_queue\_arn](#output\_s3\_bucket\_notifications\_sqs\_queue\_arn) | Notifications SQS queue ARN |
| <a name="output_s3_bucket_prefix"></a> [s3\_bucket\_prefix](#output\_s3\_bucket\_prefix) | Bucket prefix configured for lifecycle rules |
| <a name="output_s3_kms_alias_arn"></a> [s3\_kms\_alias\_arn](#output\_s3\_kms\_alias\_arn) | KMS Alias ARN |
| <a name="output_s3_kms_alias_name"></a> [s3\_kms\_alias\_name](#output\_s3\_kms\_alias\_name) | KMS Alias name |
| <a name="output_s3_kms_key_arn"></a> [s3\_kms\_key\_arn](#output\_s3\_kms\_key\_arn) | KMS Key ARN |
| <a name="output_s3_kms_key_id"></a> [s3\_kms\_key\_id](#output\_s3\_kms\_key\_id) | KMS Key ID |  
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Run project with Terraform Commands

### I. Requirements

- Have terraform cli installed
- Have aws-vault installed, see guide [here](https://github.com/99designs/aws-vault#:~:text=AWS%20Vault%20is%20a%20tool,to%20your%20shell%20and%20applications.).
- Setup aws-vault to have aws account profile

### II. Command terraform commands

1. Init project

```bash
aws-vault exec keltio --no-session -- terraform init
```

2. Generate plan

```bash
aws-vault exec keltio --no-session -- terraform plan
```

3. Apply terraform plan

```bash
aws-vault exec keltio --no-session -- terraform apply --auto-approve
```

4. Destroy resources

```bash
aws-vault exec keltio --no-session -- terraform destroy --auto-approve
```
