#!/bin/sh
if [ $(git tag -l "$VERSION") ]; then
        echo "Version $VERSION already exists"
else
        echo "Tag $VERSION with master branch"
        git checkout main
        git tag -a $VERSION -m "Version $VERSION"
        git push api-origin $VERSION
fi