output "flow_log_id" {
  description = "The Cloudwatch Flow Log ID"
  value       = {
    flow_log_id_cw = var.log_destination_type == "cloud-watch-logs" ? aws_flow_log.cw_log_group[0].id : "null"
    flow_log_id_s3 = var.log_destination_type == "s3" ? module.flow_log_s3[0].flow_log_id : "null"
  }
}

output "flow_log_arn" {
  description = "The ARN of the Flow Log"
  value       = {
    flow_log_arn_cw = var.log_destination_type == "cloud-watch-logs" ? aws_flow_log.cw_log_group[0].arn : "null"
    flow_log_arn_s3 = var.log_destination_type == "s3" ? module.flow_log_s3[0].flow_log_arn : "null"
  }
}

/* ------------------------------------------------------------------------------------------------------------ */
/* Outputs generated if Cloudwatch LogGroup is used as destination for Flowlog                                  */
/* ------------------------------------------------------------------------------------------------------------ */
output "cw_loggroup_arn" {
  description = "ARN of Cloudwatch Log group"
  value       = var.log_destination_type == "cloud-watch-logs" ? aws_cloudwatch_log_group.default[0].arn : ""
}

output "cw_loggroup_name_prefix" {
  description = "Name prefix of Cloudwatch Log group"
  value       = var.log_destination_type == "cloud-watch-logs" ? aws_cloudwatch_log_group.default[0].name_prefix : ""
}

output "cw_loggroup_name" {
  description = "Name of Cloudwatch Log group"
  value       = var.log_destination_type == "cloud-watch-logs" ? aws_cloudwatch_log_group.default[0].name : ""
}

output "cw_loggroup_retention_in_days" {
  description = "Rentention days of Cloudwatch Log group"
  value       = var.log_destination_type == "cloud-watch-logs" ? aws_cloudwatch_log_group.default[0].retention_in_days : ""
}

output "cw_loggroup_kms_key_id" {
  description = "ARN of KMS Key to use when encrypting log data of Cloudwatch Log group"
  value       = var.log_destination_type == "cloud-watch-logs" ? aws_cloudwatch_log_group.default[0].kms_key_id : ""
}

output "cw_loggroup_tags" {
  description = "Tags of Cloudwatch Log group"
  value       = var.log_destination_type == "cloud-watch-logs" ? aws_cloudwatch_log_group.default[0].tags : {}
}

/* ------------------------------------------------------------------------------------------------------------ */
/* Outputs generated if S3 Bucket is used as destination for Flowlog                                            */
/* ------------------------------------------------------------------------------------------------------------ */
output "s3_kms_key_arn" {
  value       = var.log_destination_type == "s3" ? module.flow_log_s3[0].kms_key_arn : ""
  description = "KMS Key ARN"
}

output "s3_kms_key_id" {
  value       = var.log_destination_type == "s3" ? module.flow_log_s3[0].kms_key_id : ""
  description = "KMS Key ID"
}

output "s3_kms_alias_arn" {
  value       = var.log_destination_type == "s3" ? module.flow_log_s3[0].kms_alias_arn : ""
  description = "KMS Alias ARN"
}

output "s3_kms_alias_name" {
  value       = var.log_destination_type == "s3" ? module.flow_log_s3[0].kms_alias_name : ""
  description = "KMS Alias name"
}

output "s3_bucket_domain_name" {
  value       = var.log_destination_type == "s3" ? module.flow_log_s3[0].bucket_domain_name : ""
  description = "FQDN of bucket"
}

output "s3_bucket_id" {
  value       = var.log_destination_type == "s3" ? module.flow_log_s3[0].bucket_id : ""
  description = "Bucket Name (aka ID)"
}

output "s3_bucket_arn" {
  value       = var.log_destination_type == "s3" ? module.flow_log_s3[0].bucket_arn : ""
  description = "Bucket ARN"
}

output "s3_bucket_prefix" {
  value       = var.log_destination_type == "s3" ? module.flow_log_s3[0].bucket_prefix : ""
  description = "Bucket prefix configured for lifecycle rules"
}

output "s3_bucket_notifications_sqs_queue_arn" {
  value       = var.log_destination_type == "s3" ? module.flow_log_s3[0].bucket_notifications_sqs_queue_arn : ""
  description = "Notifications SQS queue ARN"
}